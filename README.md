# mtu_lab7_printing

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - Handles adding new printers through puppet](#module-description)
3. [Setup - The basics of getting started with mtu_lab7_printing](#setup)
    * [What mtu_lab7_printing affects](#what-mtu_lab7_printing-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with mtu_lab7_printing](#beginning-with-mtu_lab7_printing)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

This module handles adding printers using cups module. It requires a list of 
printers and the names come from the ppd files that are downloaded from 
cups.it.mtu.edu

## Module Description

Has a metaclass that is passed a list and boolean and passes it to a defines
which makes new resources for each printer.

## Setup

### What mtu_lab7_printing affects

* A list of files, packages, services, or operations that the module will alter,
  impact, or execute on the system it's installed on.
* This is a great place to stick any warnings.
* Can be in list or paragraph form.

### Setup Requirements 

puppet module install maestrodev-wget
puppet module install mosen-cups

### Beginning with mtu_lab7_printing

## Usage

By default it will install husky-bw and husky-color and from cups.it.mtu.edu.
You can define printer_list as a list and on_domain as a boolean.

## Reference

https://forge.puppetlabs.com/maestrodev/wget
https://forge.puppetlabs.com/mosen/cups

## Limitations

Linux only because Windows doesn't handle printers the same

